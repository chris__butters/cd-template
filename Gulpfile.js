var gulp = require('gulp'),
sass = require('gulp-sass'),
sassGlob = require('gulp-sass-glob'),
uglify = require('gulp-uglify'),
concat = require('gulp-concat'),
cmq = require('gulp-group-css-media-queries'),
cssmin = require('gulp-cssmin'),
rename = require("gulp-rename"),
autoprefixer = require('gulp-autoprefixer'),
sourcemaps = require('gulp-sourcemaps');

gulp.task('scripts', function () {

    // Edit the source to add addition javascript files that will compile into plugin.min.js
    gulp.src('js/src/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('app.js')) //the name of the resulting file
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('js'));

    gulp.src('js/src/*.js')
        .on('error', swallowError) //select all javascript files under js/ and any subdirectory
        .pipe(concat('app.js')) //the name of the resulting file
        .on('error', swallowError) // Swallow errors so doesn't stop running
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({
          sequences     : true,  // join consecutive statemets with the “comma operator”
          properties    : true,  // optimize property access: a["foo"] → a.foo
          dead_code     : false,  // discard unreachable code
          drop_debugger : true,  // discard “debugger” statements
          unsafe        : false, // some unsafe optimizations (see below)
          conditionals  : true,  // optimize if-s and conditional expressions
          comparisons   : true,  // optimize comparisons
          evaluate      : true,  // evaluate constant expressions
          booleans      : true,  // optimize boolean expressions
          loops         : true,  // optimize loops
          unused        : true,  // drop unused variables/functions
          hoist_funs    : true,  // hoist function declarations
          hoist_vars    : false, // hoist variable declarations
          if_return     : true,  // optimize if-s followed by return/continue
          join_vars     : true,  // join var declarations
          cascade       : true,  // try to cascade `right` into `left` in sequences
          side_effects  : true,  // drop side-effect-free statements
          warnings      : true,  // warn about potentially dangerous optimizations/code
        })) // Minify
        .on('error', swallowError)
        .pipe(gulp.dest('js')) //Output directory

      
});

gulp.task('styles', function () {
    gulp.src('css/src/main.scss')
      .pipe(sassGlob())
      // .pipe(sourcemaps.init())
      .pipe(sass())
      .on('error', swallowError)
      //.pipe(gulp.dest('css'))
      .pipe(autoprefixer({
          browsers: ['last 2 versions'],
          cascade: true
      }))
      // .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('css'));

    gulp.src('css/src/main.scss')
      .pipe(sassGlob())
      .pipe(sass())
      .on('error', swallowError)
      //.pipe(gulp.dest('css'))
      .pipe(autoprefixer({
          browsers: ['last 2 versions'],
          cascade: true
      }))
      .pipe(cmq())
      .pipe(cssmin())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('css'));

});

gulp.task('default',['scripts','styles'], function(){});

gulp.task('watch', function() {
  // gulp.watch('css/src/**/*.scss', ['styles']);
  gulp.watch('css/src/**/**/*.scss', ['styles']);
  gulp.watch('components/css/**/*.scss', ['styles']);

  gulp.watch('components/js/*.js', ['scripts']);
  gulp.watch('js/src/*.js', ['scripts']);
});

// Custom error function to stop emit(end);
var swallowError = function(error) {
    console.log(error.toString());
}
