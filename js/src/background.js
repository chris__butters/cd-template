function shadeColor2(color, percent) {   
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

var base = '#993333';
var colors = [base];
var x = 25;
while (x <= 50){
	colors.push(shadeColor2(base, (x/100)*-1 ));
	x += 25;
}

var pattern = Trianglify({
  height: window.innerHeight,
  width: window.innerWidth,
  x_colors: colors,
  variance: 1,
  cell_size: 100});

// document.querySelector('.bg').appendChild(pattern.svg());